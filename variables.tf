variable "project_id" {
  default = "hypnotic-seat-309810"
}

variable "region" {
  default = "europe-north1"
}

variable "zone" {
  default = "europe-north1-a"
}

variable "cluster" {
  default = "hypnotic-seat"
}



variable "num_nodes" {
  default     = 4
  description = "number of gke nodes"
}




variable "machine_type" {
  default = "e2-standard-2"
}
variable "disk_size" {
  default = "30"
}

variable "credentials" {
  default = "./creds/serviceaccount.json"
}

variable "kubernetes_min_ver" {
  default = "latest"
}

variable "kubernetes_max_ver" {
  default = "latest"
}